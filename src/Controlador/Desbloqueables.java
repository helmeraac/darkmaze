package Controlador;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


//Verifica que los niveles esten desbloqueados
public class Desbloqueables {
	private int niveles = 3;
	private int laberintos = 3;
	private boolean[][] desbloqueado;
	private String dir = "progress.set";
	
	public Desbloqueables(){
		String[][]data = this.lectura(dir);
		for(int i = 0; i<niveles; i++){
			for(int j = 0; j<laberintos;j++){
				System.out.println(data[i][j] + " Contructor");
			}
		}
		asignacion(data);
	}

	public String[][] lectura(String dir) {
		String[][] unlock = new String[niveles][laberintos];
		// int i = 0;
		try {
			FileReader fSet = new FileReader(dir);
			BufferedReader fSetIn = new BufferedReader(fSet);
			String dato = fSetIn.readLine();
			for (int j = 0; j < niveles; j++) {
				for (int k = 0; k < laberintos; k++) {
					System.out.println(dato);
					unlock[j][k] = dato;
					dato = fSetIn.readLine();
				}
			}

			/*
			 * while (fSetIn != null && i < lineas) { unlock[i] = dato; dato =
			 * fSetIn.readLine(); i++; }
			 */

			fSetIn.close();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			for (int j = 0; j < niveles; j++) {
				for (int k = 0; k < laberintos; k++) {
					unlock[j][k] = "L";
				}
			}
			unlock[0][0] = "U";

		}
		System.out.println(unlock.toString() + " lectura");
		return unlock;
	}

	public void escritura(String dir, String[][] unlock) {
		try {
			File fSet = new File(dir);
			BufferedWriter fSetOut = new BufferedWriter(new FileWriter(fSet));
			String dato;
			for (int i1 = 0; i1 < niveles; i1++) {
				for (int j1 = 0; j1 < laberintos; j1++) {
					dato = unlock[i1][j1];
					fSetOut.write(dato);
					fSetOut.newLine();
				}
			}
			fSetOut.close();
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	public void asignacion (String[][] datos){
		desbloqueado = new boolean[niveles][laberintos];
		for(int i = 0; i<niveles; i++){
			for(int j = 0; j<laberintos;j++){
				if (datos[i][j].equalsIgnoreCase("U")){
					this.desbloqueado[i][j] = true;
				} else if (datos[i][j].equalsIgnoreCase("L")){
					this.desbloqueado[i][j] = false;
				}
			}
		}
	}
	
	public String[][] modificacion (){
		String[][] datos = new String[this.niveles][this.laberintos];
		for(int i = 0; i<niveles; i++){
			for(int j = 0; j<laberintos;j++){
				if (this.desbloqueado[i][j]){
					datos[i][j] = "U";
				} else {
					datos[i][j] = "U";
				}
			}
		}
		return datos;
	}

	public boolean[][] isDesbloqueado() {
		return desbloqueado;
	}

	public void setDesbloqueado(boolean[][] desbloqueado) {
		this.desbloqueado = desbloqueado;
	}
	
	public void close(){
		String[][] data = modificacion();
		this.escritura(dir, data);
	}
	
	
}
