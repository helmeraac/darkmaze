package Controlador;

import java.awt.event.MouseEvent;
import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;
import Modelo.Jugador;
import Modelo.Laberinto;

@SuppressWarnings("serial")
public class GUI extends PApplet {
	
	ArrayList<PImage> botones = new ArrayList<>();

	PImage bg;
	PImage bg2;
	/*PImage botones.get(0);
	PImage botones.get(1);
	PImage botones.get(2);
	PImage botones.get(3);
	PImage botones.get(4);
	PImage botones.get(5);
	PImage botones.get(6);
	PImage botones.get(7);
	PImage botones.get(8);
	PImage botones.get(9);
	PImage botones.get(10);
	PImage botones.get(11);
	PImage botones.get(12);
	PImage botones.get(13);
	PImage botones.get(14);
	PImage botones.get(15);
	PImage botones.get(16);
	*/
	PImage btR;
	PImage btE;
	PImage bL1L;
	PImage bL1U;
	PImage bL2L;
	PImage bL2U;
	PImage bL3L;
	PImage bL3U;
	PImage btD;
	PImage btP;
	PImage btC;
	PImage btM;
	
	

	String ruta = "\\Data\\";

	Ajustes conf = new Ajustes();
	boolean sonido;
	int menuLocation;
	int dificultad;
	int personaje;

	PFont font;
	String time = "000";
	int t;
	int interval = 0;
	int start;
	boolean counting = true;
	int currentTimerValue, previousTimerValue, pausedTimerValue;

	Desbloqueables unlocked = new Desbloqueables();
	boolean[][] desbloqueado;

	boolean beginJuego = false;
	Laberinto laberinto;

	float xpos = 40;
	float ypos = 40;
	float xvel = 10;
	float yvel = 10;
	float xi;
	float yi;
	float xf;
	float yf;
	Animacion animacion1, animacion2, animacion3, animacion4;

	PApplet parent;
	private Jugador jugador;

	public void setup() {
		frame.setIconImage(getToolkit().getImage("\\Data\\darkmaze.ico"));
		size(800, 600);
		frame.setTitle("Dark Maze");
		menuLocation = 1;
		sonido = false;
		dificultad = 3;
		personaje = 2;
		frameRate(12);
		font = createFont("Papyrus", 36, true);
		textFont(font);
		cargarArchivos();
		
		bg = loadImage(ruta + "Fondo.jpg");
		bg2 = loadImage(ruta + "Background.jpg");
		/*botones.get(0) = loadImage(ruta + "Boton1.png");
		botones.get(1) = loadImage(ruta + "Boton2.png");
		botones.get(2) = loadImage(ruta + "Boton3.png");
		botones.get(3) = loadImage(ruta + "Boton4.png");
		botones.get(4) = loadImage(ruta + "Boton5.png");
		botones.get(5) = loadImage(ruta + "Boton6.png");
		botones.get(6) = loadImage(ruta + "Boton7.png");
		botones.get(7) = loadImage(ruta + "Boton8.png");
		botones.get(8) = loadImage(ruta + "Boton9.png");
		botones.get(9) = loadImage(ruta + "Boton10.png");
		botones.get(10) = loadImage(ruta + "Boton11.png");
		botones.get(11) = loadImage(ruta + "Boton12.png");
		botones.get(12) = loadImage(ruta + "Boton13.png");
		botones.get(13) = loadImage(ruta + "Boton14.png");
		botones.get(14) = loadImage(ruta + "Boton15.png");
		botones.get(15) = loadImage(ruta + "Boton16.png");
		botones.get(16) = loadImage(ruta + "Boton17.png");
		*/
		btR = loadImage(ruta + "BotonR.png");
		btE = loadImage(ruta + "BotonEscoge.png");
		bL1L = loadImage(ruta + "BotonLab1L.png");
		bL1U = loadImage(ruta + "BotonLab1U.png");
		bL2L = loadImage(ruta + "BotonLab2L.png");
		bL2U = loadImage(ruta + "BotonLab2U.png");
		bL3L = loadImage(ruta + "BotonLab3L.png");
		bL3U = loadImage(ruta + "BotonLab3U.png");
		btD = loadImage(ruta + "BotonDesbloqueo.png");
		btP = loadImage(ruta + "BotonPausa.png");
		btC = loadImage(ruta + "BotonContinuar.png");
		btM = loadImage(ruta + "BotonMenu.png");
		crearAnimacion();
		// noLoop();

	}

	public void draw() {
		if (menuLocation == 1) {
			background(bg);
			image(botones.get(0), 390, 225, botones.get(0).width, botones.get(0).height);
			image(botones.get(3), 390, 325, botones.get(3).width, botones.get(3).height);
			image(botones.get(1), 390, 425, botones.get(1).width, botones.get(1).height);
			image(botones.get(2), 390, 525, botones.get(2).width, botones.get(2).height);
			mouseClicked();
		}
		if (menuLocation == 2) {

			background(bg2);
			image(btE, width / 3, 100, btE.width, btE.height);
			if (desbloqueado[dificultad - 1][0]) {
				image(bL1U, 390, 200, bL1U.width, bL1U.height);

			} else {
				image(bL1L, 390, 200, bL1L.width, bL1L.height);
			}
			if (desbloqueado[dificultad - 1][1]) {
				image(bL2U, 390, 300, bL2U.width, bL2U.height);
			} else {
				image(bL2L, 390, 300, bL2L.width, bL2L.height);
			}
			if (desbloqueado[dificultad - 1][2]) {
				image(bL3U, 390, 400, bL3U.width, bL3U.height);
			} else {
				image(bL3L, 390, 400, bL3L.width, bL3L.height);
			}
			image(botones.get(14), 600, 525, botones.get(14).width, botones.get(14).height);
			iniciarTiempo();
			
			mouseClicked();

		}
		if (menuLocation == 3) { // Ajustes
			background(bg2);
			image(botones.get(16), width / 3, 100, botones.get(16).width, botones.get(16).height);
			/*image(botones.get(4), 390, 225, botones.get(4).width, botones.get(4).height);
			if (sonido == true) {
				image(botones.get(5), 610, 225, botones.get(5).width, botones.get(5).height);
			} else {
				image(botones.get(6), 610, 225, botones.get(6).width, botones.get(6).height);
			}*/
			image(botones.get(7), 390, 325, botones.get(7).width, botones.get(7).height);
			if (dificultad == 1) {
				image(botones.get(8), 610, 325, botones.get(8).width, botones.get(8).height);
			}
			if (dificultad == 2) {
				image(botones.get(9), 610, 325, botones.get(9).width, botones.get(9).height);
			}
			if (dificultad == 3) {
				image(botones.get(10), 610, 325, botones.get(10).width, botones.get(10).height);
			}
			image(botones.get(11), 390, 425, botones.get(11).width, botones.get(11).height);
			if (personaje == 1) {
				image(botones.get(12), 610, 425, botones.get(12).width, botones.get(12).height);
			}
			if (personaje == 2) {
				image(botones.get(13), 610, 425, botones.get(13).width, botones.get(13).height);
			}
			if (personaje == 3) {
				image(btR, 610, 425, btR.width, btR.height);
			}
			image(botones.get(14), 600, 525, botones.get(14).width, botones.get(14).height);
			mouseClicked();

		}

		if (menuLocation == 4) { // Puntajes
			background(bg2);
			image(botones.get(14), 600, 525, botones.get(14).width, botones.get(14).height);
			image(botones.get(15), width / 2, 100, botones.get(15).width, botones.get(15).height);
			mouseClicked();

		}

		if (menuLocation == 5) {
			exit();
		}

		if (menuLocation == 6) {
			background(bg2);
			rect(150, 100, width - 150, height - 50);
			fill(0, 80);
			image(btD, (width / 2) - (btD.width / 2), (height / 2)
					- (btD.height / 2), btD.width, btD.height);
			image(botones.get(14), 355, 525, botones.get(14).width, botones.get(14).height);
			mouseClicked();
		}
		

		if (beginJuego) {
			laberinto.display();
			text(time, 1, 35);
			image(botones.get(14), 355, 560, botones.get(14).width, botones.get(14).height);
			jugador.mover();
		}
		if(menuLocation == 7){
			//background(bg);
			laberinto.display();
			image(btM, 390, 525, btM.width, btM.height);
			image(btP, 325, 250, btP.width, btP.height);
			mouseClicked();
			
			
			
		}
		
		

	}

	// DEsde aqui se modifica para arreglar el click del mouse
	public void mouseClicked(MouseEvent e) {
		if (mouseX > 400 && mouseX < 400 + botones.get(0).width && mouseY > 225
				&& mouseY < 225 + botones.get(0).height && menuLocation == 1) {
			// if (mousePressed) {
			start = millis();
			counting = true;
			cargarProgreso();
			menuLocation = 2;

			// }

		}
		if (mouseX > 400 && mouseX < 400 + botones.get(3).width && mouseY > 325
				&& mouseY < 325 + botones.get(3).height && menuLocation == 1) {
			// if (mousePressed) {
			menuLocation = 4;

			// }

		}
		if (mouseX > 400 && mouseX < 400 + botones.get(1).width && mouseY > 425
				&& mouseY < 425 + botones.get(1).height && menuLocation == 1) {
			// if (mousePressed) {
			cargarConfiguraciones();
			menuLocation = 3;

			// }

		}
		if (mouseX > 400 && mouseX < 400 + botones.get(2).width && mouseY > 525
				&& mouseY < 525 + botones.get(2).height && menuLocation == 1) {
			// if (mousePressed) {
			menuLocation = 5;

			// }

		}
		if (mouseX > 400 && mouseX < 400 + btM.width && mouseY > 525
				&& mouseY < 525 + btM.height && menuLocation == 7) {
			// if (mousePressed) {
			beginJuego = false;
			menuLocation = 1;
			

			// }

		}
		if (mouseX > 355 && mouseX < 355 + botones.get(14).width && mouseY > 560
				&& mouseY < 560 + botones.get(14).height && beginJuego == true)
			menuLocation=7;
		// }
		if (mouseX > 325 && mouseX < 325 + botones.get(2).width && mouseY > 250
				&& mouseY < 250 + botones.get(2).height && menuLocation == 7)
			redraw();

		// public void mouseAjustes() {
		if (mouseX > 600 && mouseX < 600 + botones.get(5).width && mouseY > 225
				&& mouseY < 225 + botones.get(5).height && menuLocation == 3) {
			// if (mousePressed) {
			if (sonido == true) {
				sonido = false;
				conf.setSonido(sonido);
			} else {
				sonido = true;
				conf.setSonido(sonido);
			}
			menuLocation = 3;
			// }
		}
		if (mouseX > 600 && mouseX < 600 + botones.get(10).width && mouseY > 325
				&& mouseY < 325 + botones.get(10).height && menuLocation == 3) {
			// if (mousePressed) {
			if (dificultad == 1) {
				dificultad = 2;
				conf.setDificultad(dificultad);
			} else if (dificultad == 2) {
				dificultad = 3;
				conf.setDificultad(dificultad);
			} else if (dificultad == 3) {
				dificultad = 1;
				conf.setDificultad(dificultad);
			}
			menuLocation = 3;
			// }
		}
		if (mouseX > 600 && mouseX < 600 + botones.get(13).width && mouseY > 425
				&& mouseY < 425 + botones.get(13).height && menuLocation == 3) {
			// if (mousePressed) {
			if (personaje == 1) {
				personaje = 2;
				conf.setPersonaje(personaje);
			} else if (personaje == 2) {
				personaje = 3;
				conf.setPersonaje(personaje);
			} else if (personaje == 3) {
				personaje = 1;
				conf.setPersonaje(personaje);
			}
			menuLocation = 3;
			// }
		}
		if (mouseX > 600 && mouseX < 600 + botones.get(14).width && mouseY > 525
				&& mouseY < 525 + botones.get(14).height && menuLocation == 3) {
			// if (mousePressed) {
			guardarConfiguraciones();
			menuLocation = 1;
			// }

		}
		// }

		// public void mousePuntajes() {
		if (mouseX > 600 && mouseX < 600 + botones.get(14).width && mouseY > 525
				&& mouseY < 525 + botones.get(14).height && menuLocation == 4) {
			// if (mousePressed) {
			menuLocation = 1;

			// }

		}

		// }

		// public void mouseJuego() {
		if (mouseX > 400 && mouseX < 400 + bL1L.width && mouseY > 200
				&& mouseY < 200 + bL1L.height && menuLocation == 2) {
			// if (mousePressed) {
			if (desbloqueado[dificultad - 1][0]) {
				laberinto = new Laberinto(dificultad, 1, this);
				xi = laberinto.getXi();
				xf = laberinto.getXf();
				yi = laberinto.getYi();
				yf = laberinto.getYf();
				jugador = new Jugador(xi, yi, conf.getPersonaje(), laberinto, this);
				beginJuego = true;
			} else {
				menuLocation = 6;
			}
			// }
		}
		if (mouseX > 400 && mouseX < 400 + bL1L.width && mouseY > 300
				&& mouseY < 300 + bL1L.height && menuLocation == 2) {
			// if (mousePressed) {
			if (desbloqueado[dificultad - 1][1]) {
				laberinto = new Laberinto(dificultad, 2, this);
				xi = laberinto.getXi();
				xf = laberinto.getXf();
				yi = laberinto.getYi();
				yf = laberinto.getYf();
				jugador = new Jugador(xi, yi, conf.getPersonaje(), laberinto, this);
				beginJuego = true;
			} else {
				menuLocation = 6;
			}
			// }
		}
		if (mouseX > 400 && mouseX < 400 + bL1L.width && mouseY > 400
				&& mouseY < 400 + bL1L.height && menuLocation == 2) {
			// if (mousePressed) {
			if (desbloqueado[dificultad - 1][2]) {
				laberinto = new Laberinto(dificultad, 3, this);
				xi = laberinto.getXi();
				xf = laberinto.getXf();
				yi = laberinto.getYi();
				yf = laberinto.getYf();
				jugador = new Jugador(xi, yi, conf.getPersonaje(), laberinto, this);
				beginJuego = true;
			} else {
				menuLocation = 6;
			}
			// }
		}
		if (mouseX > 600 && mouseX < 600 + botones.get(14).width && mouseY > 525
				&& mouseY < 525 + botones.get(14).height && menuLocation == 2) {
			// if (mousePressed) {
			start = millis();
			menuLocation = 1;

			// }

		}
		// }

		// private void mouseNo() {

		if (mouseX > 355 && mouseX < 355 + botones.get(14).width && mouseY > 525
				&& mouseY < 525 + botones.get(14).height && menuLocation == 6) {
			// if (mousePressed) {
			menuLocation = 2;
			// }

		}
	}

	// Hasta aqui se hacen los cambios para arreglar el click

	public void iniciarTiempo() {
		if (counting) {
			currentTimerValue = millis() - pausedTimerValue - start;
			time = nf(currentTimerValue / 1000, 4);

		} else {
			pausedTimerValue = millis() - currentTimerValue - start;

		}

	}

	public void pausarTiempo() {
		String tf = time;
		counting = !counting;

	}

	public void mousePressed() {
		// redraw();
		// noLoop();
	}
	
	public void crearAnimacion() {
		animacion1 = new Animacion(conf.getPersonaje() + "U", 4, this);
		animacion2 = new Animacion(conf.getPersonaje() + "D", 4, this);
		animacion3 = new Animacion(conf.getPersonaje() + "L", 4, this);
		animacion4 = new Animacion(conf.getPersonaje() + "R", 4, this);

	}

	private void cargarConfiguraciones() {
		this.sonido = conf.isSonido();
		this.dificultad = conf.getDificultad();
		// this.personaje = conf.getPersonaje();
	}

	private void guardarConfiguraciones() {
		System.out.println("guardando conf sa " + conf.isSonido());
		conf.setSonido(this.sonido);
		System.out.println("sonido d " + conf.isSonido() + "dificultad a "
				+ conf.getDificultad());
		conf.setDificultad(this.dificultad);
		System.out.println("dificultad d " + conf.getDificultad()
				+ "personaje a " + conf.getPersonaje());
		conf.setPersonaje(this.personaje);
		System.out.println("personaje d " + conf.getPersonaje());
		conf.close();
	}

	private void cargarProgreso() {
		this.desbloqueado = this.unlocked.isDesbloqueado();
	}

	private void guardarProgreso() {
		this.unlocked.setDesbloqueado(desbloqueado);
		this.unlocked.close();
	}
	
	private void cargarArchivos(){
		int k = 1;
		for(int i = 0; i < 17; i++){
			botones.add(loadImage(ruta + "Boton" + k + ".png"));
			k++;
		}
	}
}
