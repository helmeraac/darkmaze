package Controlador;

import java.io.*;

public class Ajustes {
	private final int lineas = 3; // catidad de ajustes posibles.
	private String[] conf = new String[lineas]; // arreglo para guardar los
												// ajustes.
	private final String dir = "settings.set";
	private boolean sonido; // define si hay o no sonido.
	private int dificultad; // define el nivel de dificultad.
	private int personaje = 1; // define el personaje predeterminado.

	public Ajustes() {
		this.conf = this.lectura(dir);
		asignacion(conf);
	}

	public void modificacion() {
		if (sonido == true) {
			conf[0] = "S";
		} else if (sonido == false) {
			conf[0] = "N";
		}

		if (dificultad == 1) {
			conf[1] = "F";
		} else if (dificultad == 2) {
			conf[1] = "M";
		} else if (dificultad == 3) {
			conf[1] = "D";
		}

		if (personaje == 1) {
			conf[2] = "M";
		} else if (personaje == 2) {
			conf[2] = "C";
		} else if (personaje == 3) {
			conf[2] = "R";
		}
	}

	public void asignacion(String[] conf) {

		if (conf[0].equals("S")) {
			this.setSonido(true);
		} else if (conf[0].equals("N")) {
			this.setSonido(false);
		}

		if (conf[1].equals("F")) {
			this.setDificultad(1);
		} else if (conf[1].equals("M")) {
			this.setDificultad(2);
		} else if (conf[1].equals("D")) {
			this.setDificultad(3);
		}

		if (conf[2].equals("M")) {
			this.setPersonaje(1);
		} else if (conf[2].equals("C")) {
			this.setPersonaje(2);
		} else if (conf[2].equals("R")) {
			this.setPersonaje(3);
		}
	}

	public String[] lectura(String dir) {
		String[] ajustes = new String[lineas];
		int i = 0;
		try {
			FileReader fSet = new FileReader(dir);
			BufferedReader fSetIn = new BufferedReader(fSet);
			String ajuste = fSetIn.readLine();

			while (fSetIn != null && i < lineas) {
				ajustes[i] = ajuste;
				ajuste = fSetIn.readLine();
				i++;
			}

			fSetIn.close();
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			ajustes[0] = "T";
			ajustes[1] = "M";
			ajustes[2] = "M";
		}
		return ajustes;
	}

	public void escritura(String dir, String[] ajustes) {
		try {
			File fSet = new File(dir);
			BufferedWriter fSetOut = new BufferedWriter(new FileWriter(fSet));
			String ajuste;
			for (int i1 = 0; i1 < lineas; i1++) {
				ajuste = ajustes[i1];
				fSetOut.write(ajuste);
				fSetOut.newLine();
			}
			fSetOut.close();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public boolean isSonido() {
		return sonido;
	}

	public void setSonido(boolean sonido) {
		this.sonido = sonido;
	}

	public int getDificultad() {
		
		return dificultad;
	}

	public void setDificultad(int dificultad) {
		this.dificultad = dificultad;
	}

	public String getPersonaje() {
		String personajeString = null;
		if(personaje == 1){
			personajeString = "mike";
		}else if(personaje == 2){
			personajeString = "chris";
		}else if(personaje == 3){
			personajeString = "robin";
			
		}
		return personajeString;
	}

	public void setPersonaje(int personaje) {
		this.personaje = personaje;
	}

	public void close() {
		modificacion();
		this.escritura(dir, conf);
	}
}
