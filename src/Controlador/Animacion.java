package Controlador;

import processing.core.PApplet;
import processing.core.PImage;

//Todos los objetos que se muevan en pantalla llamaran a esta clase y cada uno lo har� a su manera.

public class Animacion{
	PApplet parent;
	PImage[] images;
	int imageCount;
	int frame = 1;
	Ajustes conf = new Ajustes();
	
	String ruta;
	
		public Animacion(String imagePrefix, int count, PApplet p) {
			if(conf.getPersonaje() == "mike"){
				ruta = "\\Data\\Mike\\";
			}else if(conf.getPersonaje() == "chris"){
				ruta = "\\Data\\Chris\\";
			}else if(conf.getPersonaje() == "robin"){
				ruta = "\\Data\\Robin\\";
			}
		
		parent = p;
		imageCount = count;
		images = new PImage[imageCount];
					
				
		images[0] = parent.loadImage(ruta + imagePrefix + ".png");
		images[1] = parent.loadImage(ruta + imagePrefix + "1.png");
		images[2] = parent.loadImage(ruta + imagePrefix + ".png");
		images[3] = parent.loadImage(ruta + imagePrefix + "2.png");
	}

	public void display(float xpos, float ypos) {
		frame = (frame + 1) % imageCount;
		parent.image(images[frame], xpos, ypos);
	}

	public int getWidth() {
		return images[0].width;
	}
	public int getHeight(){
		return images[0].height;
	}
	
	public void getFrame (float xpos, float ypos){
		parent.image(images[0], xpos, ypos);
	}

}
