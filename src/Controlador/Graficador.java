package Controlador;

import java.awt.*;

//Todos los objetos que se deban graficar en pantalla implementaran esta interface y cada uno lo har� a su manera.
public interface Graficador {
	
	public void display();
}
