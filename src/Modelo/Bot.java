package Modelo;


//Clase del robot "intelligent"

public class Bot extends Personaje{

	private float x; // posiciones
	private float y;
	private final int vx = 10; // Velocidades
	private final int vy = 10;
	private String recurso;

	public Bot(int xi, int yi) {
		super(xi, yi);
		this.x = xi;
		this.y = yi;
	}


	
	public float getX() {
		return x;
	}



	public void setX(float x) {
		this.x = x;
	}



	public float getY() {
		return y;
	}



	public void setY(float y) {
		this.y = y;
	}



	public void animar() {

	}

	
	public void display() {

	}

	@Override
	public void mover() {
		
	}


}
