package Modelo;

import java.io.*;
import java.util.ArrayList;

import processing.core.*;

public class Laberinto {

	private String dir;
	private String background;
	private String ladrillo;
	private String level;
	private PImage fondo;
	private ArrayList<Celda> laberinto = new ArrayList<Celda>();
	private boolean desbloqueado = false;
	private PApplet parent;
	private int dificultad;
	private int nivel;
	private float xi;
	private float yi;
	private float xf;
	private float yf;
	private int xm;
	private int ym;
	
	private boolean[][] recorrible;

	public Laberinto(int dificultad, int nivel, PApplet p) {
		this.dificultad = dificultad;
		this.nivel = nivel;
		this.parent = p;
		this.dir = "src\\Data\\Mazes\\";
		this.background = this.dir + "Background" + dificultad + nivel + ".jpg";
		this.level = this.dir + "Level" + dificultad + nivel + ".maz";
		this.fondo = parent.loadImage(this.background);
		llenar();
	}

	public void llenar() {
		int x; //variables de posicion de celda
		int y;
		int xm;
		int ym;
		char d;
		boolean ciclo;

		try {

			x = 0;
			y = 0;
			xm = 1;
			ym = 1;
			FileReader fLab = new FileReader(level);
			BufferedReader fLabIn = new BufferedReader(fLab);
			String caracter = "L";

			ciclo = true;
			while (ciclo) {
				
				caracter = fLabIn.readLine();
				
				if (caracter == null){
					break;
				}

				switch (caracter) {

				case "P":
					laberinto.add(new Pared(x, y, parent)); // a�ade una pared al
															// arreglo de
															// elementos del
															// laberinto
					break;

				case "L":
					laberinto.add(new Espacio(x, y, parent)); // A�ade un espacio
															// vacio al arreglo
					break;

				case "E":
					laberinto.add(new Inicio(x, y, parent)); // A�ade una entrada al arreglo
					xi = x;
					yi = y;
					break;

				case "S":
					laberinto.add(new Final(x, y, parent)); // A�ade una salida al
															// arreglo
					xf = x;
					yf = y;
					break;

				default:
					ciclo = false;
					break;
				}

				x+=40;
				this.xm = xm;
				this.ym = ym;
				xm += 1;
				if (x == 800) {
					x = 0;
					y+=40;
					xm = 1;
					ym += 1;
				}
				
			}
			fLabIn.close();
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (NullPointerException e){
			System.out.println(e.getMessage());
		}
	}

	public float getXi() {
		return xi;
	}

	public float getYi() {
		return yi;
	}

	public float getXf() {
		return xf;
	}

	public float getYf() {
		return yf;
	}

	public void display() {
		parent.background(this.fondo);
		/*for(Celda i : laberinto){
			i.display();
		}*/
		
	}

	public int getXm() {
		return xm;
	}

	public int getYm() {
		return ym;
	}

	public boolean[][] getRecorrible() {
		int k = 0; //indice en el arrayList de celdas
		recorrible = new boolean[this.ym][this.xm];
		for (int i = 0; i < this.ym ; i++){
			for (int j = 0; j < this.xm; j++){
				recorrible[i][j] = laberinto.get(k).isRecorrible();
				System.out.println(recorrible[i][j]);
				k += 1;
			}
		}
		return recorrible;
	}

}
