package Modelo;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import Comparador.PuntajeComp;
import Controlador.Temporizador;

public class Puntaje {

	private String nombre;
	private int tiempo;
	private int puntuacion;
	private Puntaje Puntaje;

	public Puntaje(String nombre, int tiempo, int puntuacion) {

		this.nombre = nombre;
		this.tiempo = tiempo;
		this.puntuacion = puntuacion;
	}

	Set<Puntaje> listaPuntajes = new TreeSet<>(new PuntajeComp());
	File archivo1 = new File("Puntajes.txt");

	public int getTiempo() {
		return tiempo;
	}

	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPuntuacion() {
		return puntuacion;
	}

	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}

	
	public void eliminarPuntaje() {
		listaPuntajes.remove(this.Puntaje);
		// elimina el puntaje seleccionado
	}

	public void reiniciarPuntajes() {
		listaPuntajes.clear();
		// elimina todos los puntajes guardados
	}

	public void guardarPuntaje() {
		listaPuntajes.add(this.Puntaje);

		// guarda el puntaje actual en la lista de puntajes( se hara con
		// treeset)
	}

	public void mostrarPuntajes() {
		Puntaje.leerArchivo("Puntajes.txt");
		// lee el txt y muestra los puntajes en pantalla
	}

	

	public static boolean guardarArchivo(String filepath, TreeSet<Puntaje> filelines){
		try{
			PrintWriter wr = new PrintWriter(filepath);
			for(Puntaje line : filelines){
				wr.println(line);
			}
			wr.close();
		}catch(FileNotFoundException e){
			System.err.println("Error en Puntaje.guardarArchivo("+filepath+");");
			e.printStackTrace();
		}
		return true;
	}
	
	
	public static ArrayList<String> leerArchivo(String filepath) {
		ArrayList<String> answer = new ArrayList<String>();

		File archivo = new File(filepath);
		if (archivo.exists()) {
			try {
				Scanner sn;
				sn = new Scanner(archivo);

				while (sn.hasNextLine())
					answer.add(sn.nextLine());

				sn.close();
			} catch (FileNotFoundException e) {
				System.err.println("Error en Puntaje.leerArchivo(" + filepath
						+ ");");
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Archivo " + filepath + " no existe");
			return null;
		}
		return answer;
	}

}
