package Modelo;

import java.awt.Graphics;
import java.awt.Image;

import processing.core.PApplet;

public class Espacio extends Celda {
	
	private String recurso;

	public Espacio(int posX, int posY, PApplet p) {
		super(posX, posY);
	}

	public void display() {
		
	}

	public boolean isRecorrible() {
		return true;
	}

}
