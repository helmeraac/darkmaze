package Modelo;

import java.awt.Graphics;
import java.awt.Image;

import processing.core.*;
import Controlador.Graficador;

public abstract class Celda implements Graficador {
	
	private int x, y;
	private static int dificultad;
	private static int nivel;
	private static PApplet parent;

	public Celda(int posX, int posY) {
		this.setX(posX);
		this.setY(posY);
	}


	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public abstract boolean isRecorrible();
	
	public abstract void display();

}
