package Modelo;

import Controlador.Animacion;
import Controlador.Graficador;

//Clase para los personajes

public abstract class Personaje implements Graficador{

	private float x; // posiciones
	private float y;
	private float vx; // Velocidades
	private float vy;


	public Personaje(float xi, float yi) {
		this.x = xi;
		this.y = yi;
	}
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public abstract void display();

	public abstract void animar();
	
	public abstract void mover();
}
