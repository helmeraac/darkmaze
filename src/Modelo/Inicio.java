package Modelo;

import java.awt.Graphics;

import processing.core.PApplet;

public class Inicio extends Celda {
	
	private String recurso;
	private PApplet parent;
	private int x, y;

	public Inicio(int posX, int posY, PApplet p) {
		super(posX, posY);
		this.x = posX;
		this.y = posY;
		this.parent = p;
	}


	public void display() {
		
	}


	public boolean isRecorrible() {
		return true;
	}


}
