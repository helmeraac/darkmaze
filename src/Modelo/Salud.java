package Modelo;

import java.awt.*;

public class Salud extends Modificador{

	private String recurso;
	private int x;
	private int y;
	
	public void modificar(Jugador j) {
		if (j.getX() == this.x && j.getY() == this.y){
			j.setVidas(1);
		}
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}


	public void display() {
	}

}
