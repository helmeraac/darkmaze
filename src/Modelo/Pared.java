package Modelo;

import java.awt.Graphics;
import java.awt.Image;

import processing.core.*;
import Controlador.Graficador;

public class Pared extends Celda {

	private int x, y;
	// private String recurso;
	private PApplet parent;

	// private PImage ladrillo;

	public Pared(int posX, int posY, PApplet p) {
		super(posX, posY);
		this.x = posX;
		this.y = posY;
		this.parent = p;
		// recurso = "\\Data\\Mazes\\Ladrillo" + dificultad + ".jpg";
	}

	public void display() {
		/*
		 * ladrillo = parent.loadImage(recurso); F parent.image(ladrillo,
		 * this.x,this.y, ladrillo.width, ladrillo.height);
		 */
	}

	public boolean isRecorrible() {
		return false;
	}

}
