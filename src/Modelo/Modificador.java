package Modelo;

import java.awt.Graphics;

import Controlador.Graficador;

public abstract class Modificador implements Graficador{
	
	public Modificador(){
		
	}
	
	public abstract void modificar(Jugador j);

	
}
