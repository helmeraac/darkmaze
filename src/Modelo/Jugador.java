package Modelo;

import java.awt.Graphics;
import java.awt.Image;

import Controlador.Animacion;
import processing.core.PApplet;
import processing.core.PConstants;

public class Jugador extends Personaje {
	private String nombre;
	private String personaje; // variable para manipular el personaje durante el
								// juego.
	private int vidas;
	private int puntaje;
	// Direccion de la imagen a usar dependiendo del
	// personaje.
	private String recurso;
	private boolean vivo; // Estado del jugador, si esta vivo o muerto.
	private float x; // posiciones
	private float y;
	private float vx; // Velocidades
	private float vy;
	private PApplet parent;
	private boolean[][] recorribles; // guarda si las celdas son recorribles.
	Animacion animacion1, animacion2, animacion3, animacion4;
	//private int anteriorX;
	//private int anteriorY;

	public Jugador(float xi, float yi, String p, Laberinto lab, PApplet parent) {
		super(xi, yi);
		this.x = xi;
		this.y = yi;
		this.vx = 5;
		this.vy = 5;
		this.personaje = p;
		this.parent = parent;
		//anteriorX = (int) (xi/40);
		//anteriorY = (int) (yi/40);
		this.recorribles = new boolean[lab.getYm()][lab.getXm()];
		this.recorribles = lab.getRecorrible();
		creacion();
	}

	public void creacion() {
		animacion1 = new Animacion(personaje + "U", 4, parent);
		animacion2 = new Animacion(personaje + "D", 4, parent);
		animacion3 = new Animacion(personaje + "L", 4, parent);
		animacion4 = new Animacion(personaje + "R", 4, parent);
		this.setVidas(4);
	}

	public void comer(Modificador objeto) { // Dependiendo de la fruta o un
											// objeto cambiar un atributo. Puede
											// ser puntaje
		objeto.modificar(this);

	}

	public void serComido(Jugador j, Bot a, Inicio i) {
		if (j.getX() == a.getX() && j.getY() == a.getY()) {
			j.setVidas(j.getVidas() - 1); // le quita una vida
			j.setX(i.getX()); // cambia la posicion al principio del laberinto
			j.setY(i.getY());
		}

	}

	public void mover() {
		if (parent.keyCode == PConstants.UP) {
			if (parent.keyPressed) {
				if (y > 0
						&& recorribles[this.getCeldaY((int) (y/40) + 1)][this
								.getCeldaX((int) (x/40))]) {
					y = y - vy;
					animacion1.display(x, y);
				} else {
					animacion1.getFrame(x, y);
				}
			
			} else {
				animacion1.getFrame(x, y);
			}
			

		} else if (parent.keyCode == PConstants.DOWN) {
			if (parent.keyPressed) {
				if (y < (parent.height - animacion4.getHeight())
						&& recorribles[this.getCeldaY((int) (y/40) - 1)][this
								.getCeldaX((int) (x/40))]) {
					y = y + vy;
					animacion2.display(x, y);
				} else {
					animacion2.getFrame(x, y);
				}
			} else {
				animacion2.getFrame(x, y);
			}
			

		} else if (parent.keyCode == PConstants.LEFT) {
			if (parent.keyPressed) {
				if (x > 0
						&& recorribles[this.getCeldaY((int) (y/40))][this
								.getCeldaX((int) (x/40) + 1)]) {
					x = x - vx;
					animacion3.display(x, y);
				} else {
					animacion3.getFrame(x, y);
				}
			} else {
				animacion3.getFrame(x, y);
			}
			

		} else if (parent.keyCode == PConstants.RIGHT) {
			if (parent.keyPressed) {
				if (x < (parent.width - animacion4.getWidth())
						&& recorribles[this.getCeldaY((int) (y/40))][this
								.getCeldaX((int) (x/40) - 1)]) {
					x = x + vx;
					animacion4.display(x, y);
				} else {
					animacion4.getFrame(x, y);
				}
			} else {
				animacion4.getFrame(x, y);
			}


		} else {
			animacion2.getFrame(x, y);
		}
	}

	public void animar() {

	}

	public void display() {

	}

	public String getNombre() {
		return nombre;
	}

	public int getVidas() {
		return vidas;
	}

	public void setVidas(int vidas) {
		if (this.getVidas() <= 5) {
			this.vidas += vidas;
		}
		if (this.vidas == 1 && vidas == -1) {
			this.vidas = 0;
			this.setVivo(false);
		}
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public boolean isVivo() {
		return vivo;
	}

	public void setVivo(boolean vivo) {
		this.vivo = vivo;
	}

	public int getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}

	public int getCeldaX(int anterior) { // Toma el valor de la celda anterior y
											// verifica si cambi� de celda
		int c = (int) (x / 40);
		if ((int) (x / 40) < (int) ((x + animacion3.getWidth()) / 40)) {
			if (anterior < (x / 40)) {
				c = (int) ((x + animacion3.getWidth()) / 40);
			}
		}
		return c;
	}

	public int getCeldaY(int anterior) {
		int c = (int) (y / 40);
		if ((int) (y / 40) < (int) ((y + animacion3.getHeight()) / 40)) {
			if (anterior < (y  / 40)) {
				c = (int) ((y + animacion3.getHeight()) / 40);
			}
		}
		return c;
	}

}
