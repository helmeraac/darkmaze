package Controlador;

import processing.core.*;
import processing.core.PImage;

public class GUI extends PApplet{
	
	PImage bg;
	PImage bt1;
	PImage bt2;
	PImage bt3;
	PImage bt4;
	PImage bt5;
	PImage bt6;
	PImage bt7;
	PImage bt8;
	PImage bt9;
	PImage bt10;
	PImage bt11;
	PImage bt12;
	PImage bt13;
	PImage bt14;
	PImage bt15;
	PImage bt16;
	PImage bt17;
	String ruta = "\\Data\\";
	Ajustes conf = new Ajustes();
	boolean sonido;
	int menuLocation;
	int dificultad;
	int personaje;
	PApplet parent;
	
	
	public void setup() {
		size(800, 600);
		menuLocation = 1;
		sonido = false;
		dificultad = 3;
		personaje = 1;
		bg = loadImage(ruta + "Fondo.jpg");
		bt1 = loadImage(ruta + "Boton1.jpg");
		bt2 = loadImage(ruta + "Boton2.jpg");
		bt3 = loadImage(ruta + "Boton3.jpg");
		bt4 = loadImage(ruta + "Boton4.jpg");
		bt5 = loadImage(ruta + "Boton5.jpg");
		bt6 = loadImage(ruta + "Boton6.jpg");
		bt7 = loadImage(ruta + "Boton7.jpg");
		bt8 = loadImage(ruta + "Boton8.jpg");
		bt9 = loadImage(ruta + "Boton9.jpg");
		bt10 = loadImage(ruta + "Boton10.jpg");
		bt11 = loadImage(ruta + "Boton11.jpg");
		bt12 = loadImage(ruta + "Boton12.jpg");
		bt13 = loadImage(ruta + "Boton13.jpg");
		bt14 = loadImage(ruta + "Boton14.jpg");
		bt15 = loadImage(ruta + "Boton15.jpg");
		bt16 = loadImage(ruta + "Boton16.png");
		bt17 = loadImage(ruta + "Boton17.png");
		// noLoop();

	}

	
	public void draw() {

		if (menuLocation == 1) {
			background(bg);
			image(bt1, 400, 225, bt1.width, bt1.height);
			image(bt4, 400, 325, bt4.width, bt4.height);
			image(bt2, 400, 425, bt2.width, bt2.height);
			image(bt3, 400, 525, bt3.width, bt3.height);
			mousePrincipal();
		}
		if (menuLocation == 2) {
			background(bg);
			// Aqui va la interfaz del laberinto

		}
		if (menuLocation == 3) { // Ajustes
			background(bg);
			image(bt17, width / 2, 100, bt17.width, bt17.height);
			image(bt5, 400, 225, bt5.width, bt5.height);
			if (sonido == true) {
				image(bt6, 600, 225, bt6.width, bt6.height);
			} else {
				image(bt7, 600, 225, bt7.width, bt7.height);
			}
			image(bt8, 400, 325, bt8.width, bt8.height);
			if (dificultad == 1) {
				image(bt9, 600, 325, bt9.width, bt9.height);
			}
			if (dificultad == 2) {
				image(bt10, 600, 325, bt10.width, bt10.height);
			}
			if (dificultad == 3) {
				image(bt11, 600, 325, bt11.width, bt11.height);
			}
			image(bt12, 400, 425, bt12.width, bt12.height);
			if (personaje == 1) {
				image(bt13, 600, 425, bt13.width, bt13.height);
			}
			if (personaje == 2) {
				image(bt14, 600, 425, bt14.width, bt14.height);
			}
			if (personaje == 3) {
				image(bt14, 600, 425, bt14.width, bt14.height);
			}
			image(bt15, 600, 525, bt15.width, bt15.height);
			mouseAjustes();

		}

		if (menuLocation == 4) { // Puntajes
			background(bg);
			image(bt15, 600, 525, bt15.width, bt15.height);
			image(bt16, width / 2, 100, bt16.width, bt16.height);
			mousePuntajes();

		}

		if (menuLocation == 5) {
			exit();
		}

	}

	public void mousePrincipal() {
		if (mouseX > 400 && mouseX < 400 + bt1.width && mouseY > 225
				&& mouseY < 225 + bt1.height) {
			if (mousePressed) {
				menuLocation = 2;

			}

		}
		if (mouseX > 400 && mouseX < 400 + bt4.width && mouseY > 325
				&& mouseY < 325 + bt4.height) {
			if (mousePressed) {
				menuLocation = 4;

			}

		}
		if (mouseX > 400 && mouseX < 400 + bt2.width && mouseY > 425
				&& mouseY < 425 + bt2.height) {
			if (mousePressed) {
				cargarConfiguraciones();
				menuLocation = 3;

			}

		}
		if (mouseX > 400 && mouseX < 400 + bt3.width && mouseY > 525
				&& mouseY < 525 + bt3.height) {
			if (mousePressed) {
				menuLocation = 5;

			}

		}

	}

	public void mouseAjustes() {
		if (mouseX > 400 && mouseX < 400 + bt5.width && mouseY > 225
				&& mouseY < 225 + bt5.height) {
			if (mousePressed) {
				if (sonido == true) {
					sonido = false;
				} else {
					sonido = true;
				}
				menuLocation = 3;
			}
		}
		if (mouseX > 400 && mouseX < 400 + bt8.width && mouseY > 325
				&& mouseY < 325 + bt8.height) {
			if (mousePressed) {
				if (dificultad == 1) {
					dificultad = 2;
				} else if (dificultad == 2) {
					dificultad = 3;
				} else if (dificultad == 3) {
					dificultad = 1;
				}
				menuLocation = 3;
			}
		}
		if (mouseX > 400 && mouseX < 400 + bt12.width && mouseY > 425
				&& mouseY < 425 + bt12.height) {
			if (mousePressed) {
				if (personaje == 1) {
					personaje = 2;
				} else if (personaje == 2) {
					personaje = 3;
				} else if (personaje == 3) {
					personaje = 1;
				}
				menuLocation = 3;
			}
		}
		if (mouseX > 600 && mouseX < 600 + bt15.width && mouseY > 525
				&& mouseY < 525 + bt15.height) {
			if (mousePressed) {
				guardarConfiguraciones();
				menuLocation = 1;
			}

		}
	}

	public void mousePuntajes() {
		if (mouseX > 600 && mouseX < 600 + bt15.width && mouseY > 525
				&& mouseY < 525 + bt15.height) {
			if (mousePressed) {
				menuLocation = 1;

			}

		}

	}

	public void mousePressed() {
		redraw();
		// noLoop();
	}
	
	private void cargarConfiguraciones() {
		this.sonido = conf.isSonido();
		this.dificultad = conf.getDificultad();
		this.personaje = conf.getPersonaje();
	}
	
	private void guardarConfiguraciones(){
		conf.setSonido(this.sonido);
		conf.setDificultad(this.dificultad);
		conf.setPersonaje(this.personaje);
		conf.close();
	}
}
