package Controlador;

import java.io.*;

public class Ajustes {
	private final int lineas = 3; // catidad de ajustes posibles.
	private String[] conf = new String[lineas]; // arreglo para guardar los
												// ajustes.
	private final String dir = "settings.set";
	private boolean sonido; // define si hay o no sonido.
	private int dificultad; // define el nivel de dificultad.
	private int personaje; // define el personaje predeterminado.

	public Ajustes() {
		this.conf = this.lectura(dir);
		asignacion(conf);
		System.out.println(sonido + " " + dificultad + " " + personaje + "");
	}

	public void modificacion() {
		if (sonido == true) {
			conf[0] = "S";
		} else if (sonido == false) {
			conf[0] = "N";
		}

		if (dificultad == 0) {
			conf[1] = "F";
		} else if (dificultad == 1) {
			conf[1] = "M";
		} else if (dificultad == 2) {
			conf[1] = "D";
		}

		if (personaje == 0) {
			conf[2] = "M";
		} else if (personaje == 1) {
			conf[2] = "C";
		} else if (personaje == 2) {
			conf[2] = "R";
		}
	}

	public void asignacion(String[] conf) {

		if (conf[0].equals("S")) {
			this.setSonido(true);
		} else if (conf[0].equals("N")) {
			this.setSonido(false);
		}

		if (conf[1].equals("F")) {
			this.setDificultad(1);
		} else if (conf[1].equals("M")) {
			this.setDificultad(2);
		} else if (conf[1].equals("D")) {
			this.setDificultad(3);
		}

		if (conf[2].equals("M")) {
			this.setPersonaje(1);
		} else if (conf[2].equals("C")) {
			this.setPersonaje(2);
		} else if (conf[2].equals("R")) {
			this.setPersonaje(3);
		}
	}

	public String[] lectura(String dir) {
		String[] ajustes = new String[lineas];
		int i = 0;
		try {
			FileReader fSet = new FileReader(dir);
			BufferedReader fSetIn = new BufferedReader(fSet);
			String ajuste = fSetIn.readLine();

			while (fSetIn != null && i < lineas) {
				ajustes[i] = ajuste;
				ajuste = fSetIn.readLine();
				i++;
			}

			fSetIn.close();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			ajustes[0] = "T";
			ajustes[1] = "M";
			ajustes[3] = "M";
		}
		return ajustes;
	}

	public void escritura(String dir, String[] ajustes) {
		try {
			System.out.println(sonido + " " + dificultad + " " + personaje + "");
			System.out.println(ajustes[0] + ajustes[1] + ajustes[2]);
			System.out.println("Abriendo archivo");
			File fSet = new File(dir);
			System.out.println("Abriendo Bufer");
			BufferedWriter fSetOut = new BufferedWriter(new FileWriter(fSet));
			String ajuste;
			System.out.println("entrando al for");
			for (int i1 = 0; i1 < lineas; i1++) {
				ajuste = ajustes[i1];
				System.out.println(ajuste);
				fSetOut.write(ajuste);
				fSetOut.newLine();
			}
			System.out.println("cerrando archivo");
			fSetOut.close();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public boolean isSonido() {
		return sonido;
	}

	public void setSonido(boolean sonido) {
		this.sonido = sonido;
	}

	public int getDificultad() {
		return dificultad;
	}

	public void setDificultad(int dificultad) {
		this.dificultad = dificultad;
	}

	public int getPersonaje() {
		return personaje;
	}

	public void setPersonaje(int personaje) {
		this.personaje = personaje;
	}

	public void close() {
		System.out.println("cerrando");
		System.out.println(sonido + " " + dificultad + " " + personaje + "");
		modificacion();
		System.out.println("entrando a escritura");
		this.escritura(dir, conf);
	}
}
