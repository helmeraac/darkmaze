package Controlador;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import processing.core.*;
import Modelo.*;

public class Juego extends JFrame implements ActionListener{
	
	public static void main(String[] args) {
		PApplet.main("Controlador.GUI");
		
	}

	/*
	 * Verifica la continuidad del juego. devuelve 0 si el juego continua.
	 * devueve 1 si gana devuelve 2 si pierde.
	 */
	public static int estado(Jugador j, Final e) {
		if (j.isVivo())
			return 0;
		else if (j.getX() == e.getX() && j.getY() == e.getY())
			return 1;
		else
			return 2;
	}

	public void actionPerformed(ActionEvent e) {
	}


}
